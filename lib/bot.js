var xmpp = require('node-xmpp');
var events = require('events');
var util = require('util');
var bind = require('./bind');

var Bot = function(options) {
    var self = this;
    this.options = options;
    this.name = null;
    this.mention_name = null;
    this.iq_count = 1;
    this.keepalive = null;
    this.mucHost = this.options.host ? "conf." + this.options.host : "conf.hipchat.com";

    events.EventEmitter.call(this);
    this.connect();
}

util.inherits(Bot, events.EventEmitter);

Bot.prototype.connect = function() {
    var self = this;
    this.xmppClient = new xmpp.Client({
        jid: this.options.jid + '/bot',
        password: this.options.password,
        host: this.options.host
    });

    this.xmppClient.on('online', bind(this.onOnline, this));
    this.xmppClient.on('stanza', bind(this.onStanza, this));
    this.xmppClient.on('error', bind(this.onError, this));
}

Bot.prototype.getProfile = function(callback) {
    var stanza = new xmpp.Element('iq', { type: 'get' })
                 .c('vCard', { xmlns: 'vcard-temp' });
    this.sendIq(stanza, function(err, response) {
        var data = {};
        if (!err) {
            var fields = response.getChild('vCard').children;
            fields.forEach(function(field) {
                data[field.name.toLowerCase()] = field.getText();
            });
        }
        callback(err, data, response);
    });
}

Bot.prototype.sendIq = function(stanza, callback) {
    stanza = stanza.root();
    var id = this.iq_count++;
    stanza.attrs.id = id;
    this.once('iq:' + id, callback);
    this.xmppClient.send(stanza);
}

Bot.prototype.join = function(roomJid, historyStanzas) {
    if (!historyStanzas) {
        historyStanzas = 0;
    }
    var packet = new xmpp.Element('presence', { to: roomJid + '/' + this.name });
    packet.c('x', { xmlns: 'http://jabber.org/protocol/muc' })
          .c('history', {
            xmlns: 'http://jabber.org/protocol/muc',
            maxstanzas: String(historyStanzas)
          });
    this.xmppClient.send(packet);
}

Bot.prototype.setAvailability = function(availability, status) {
    var packet = new xmpp.Element('presence', { type: 'available' });
    packet.c('show').t(availability);

    if (status) {
      packet.c('status').t(status);
    }

    packet.c('c', {
      xmlns: 'http://jabber.org/protocol/caps',
      node: 'http://hipchat.com/client/bot',
      ver: 'caitsith-hipchatbot:1.0'
    });

    this.xmppClient.send(packet);
}

Bot.prototype.message = function(targetJid, message) {
    var packet;
    var parsedJid = new xmpp.JID(targetJid);

    if (parsedJid.domain === this.mucHost) {
      packet = new xmpp.Element('message', {
        to: targetJid + '/' + this.name,
        type: 'groupchat'
      });
    } else {
      packet = new xmpp.Element('message', {
        to: targetJid,
        type: 'chat',
        from: this.options.jid
      });
      packet.c('inactive', { xmlns: 'http://jabber/protocol/chatstates' });
    }

    packet.c('body').t(message);
    this.xmppClient.send(packet);
}

Bot.prototype.disconnect = function() {
    if (this.keepalive) {
      clearInterval(this.keepalive);
      this.keepalive = null;
    }
    this.xmppClient.end();
    this.emit('disconnect');
}

Bot.prototype.onOnline = function() {
    var self = this;
    self.setAvailability('chat');

    this.keepalive = setInterval(function() {
        self.xmppClient.send(new xmpp.Message({}));
    }, 30000);

    self.getProfile(function(err, data) {
        if (err) {
            self.emit('error', null, 'Unable to get profile info: ' + err, null);
            return;
        }

        self.name = data.fn;
        self.mention_name = data.nickname;
        self.emit('connect');
   });
}

Bot.prototype.onStanza = function(stanza) {
    this.emit('data', stanza);

    if (stanza.is('message') && stanza.attrs.type === 'groupchat') {
        var body = stanza.getChildText('body');
        if (!body || stanza.getChild('delay')) return;

        var jid = new xmpp.JID(stanza.attrs.from);
        var channel = jid.bare().toString();
        var sender = jid.resource;

        if (sender === this.name) return;

        this.emit('message', channel, sender, body);
    } else if (stanza.is('message') && stanza.attrs.type === 'chat') {
      // Message without body is probably a typing notification
      var body = stanza.getChildText('body');
      if (!body) return;

      var fromJid = new xmpp.JID(stanza.attrs.from);

      this.emit('privateMessage', fromJid.bare().toString(), body);
    } else if (stanza.is('message') && !stanza.attrs.type) {
      // TODO: It'd be great if we could have some sort of xpath-based listener
      // so we could just watch for '/message/x/invite' stanzas instead of
      // doing all this manual getChild nonsense.
      var x = stanza.getChild('x', 'http://jabber.org/protocol/muc#user');
      if (!x) return;
      var invite = x.getChild('invite');
      if (!invite) return;
      var reason = invite.getChildText('reason');

      var inviteRoom = new xmpp.JID(stanza.attrs.from);
      var inviteSender = new xmpp.JID(invite.attrs.from);

      this.emit('invite', inviteRoom.bare(), inviteSender.bare(), reason);
    } else if (stanza.is('iq')) {
        // Handle a response to an IQ request
        var event_id = 'iq:' + stanza.attrs.id;
        if (stanza.attrs.type === 'result') {
            this.emit(event_id, null, stanza);
        } else {
            // IQ error response
            // ex: http://xmpp.org/rfcs/rfc6121.html#roster-syntax-actions-result
            var condition = 'unknown';
            var error_elem = stanza.getChild('error');
            if (error_elem) {
                condition = error_elem.children[0].name;
            }
            this.emit(event_id, condition, stanza);
        }
    }
}

Bot.prototype.onError = function(error) {
    if (error instanceof xmpp.Element) {
        var condition = error.children[0].name;
        var text = error.getChildText('text');
        if (!text) {
            text = "No error text sent by HipChat, see "
               + "http://xmpp.org/rfcs/rfc6120.html#streams-error-conditions"
               + " for error condition descriptions.";
        }

        this.emit('error', condition, text, error);
    } else {
        this.emit('error', null, null, error);
    }
}

module.exports = Bot;
