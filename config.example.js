module.exports = {
  xmpp: {
    jid: 'xxxx_xxxx@chat.hipchat.com',
    password: 'xxxxxx',
    defaultChannel: 'xxxxx@conf.hipchat.com'
  },
  rooms: [{
    id: 'xxxx', // API ID
    jid: 'xxxx@conf.hipchat.com'
  }],
  token: 'xxxxx',
  defaultRoom: xxxxxx,
  autoLink: true
};
