var log4js = require('log4js');
var WebSocket = require('ws');
var request = require('request');
var fs = require('fs');
var child_process = require('child_process');
var dgram = require('dgram');
var Bot = require('./lib/bot');
var bind = require('./lib/bind');
var config = require('./config');

App = function(config) {
    var self = this;
    this.config = config;
    this.logger = log4js.getLogger();
    this.bot = null;
    this.socket = null;
    this.receivedIds = [];
}

App.prototype.run = function() {
    var self = this;
    this.bot = new Bot({
        jid: this.config.xmpp.jid,
        password: this.config.xmpp.password
    });
    this.bot.on('connect', function() {
        self.logger.info('connected.');

        for (var i = 0; i < self.config.rooms.length; i++) {
            var room = self.config.rooms[i];
            self.bot.join(room.jid);
        }
    });
    this.bot.on('message', bind(this.onBotMessage, this));
    this.bot.on('error', function(error, text, stanza) {
        self.logger.warn(error, text, stanza);
    });

    this.socket = dgram.createSocket('udp4');
    this.socket.bind(27070, function() {
    });
    this.socket.on('message', bind(this.onDatagramReceived, this));
}

App.prototype.onBotMessage = function(channel, sender, body) {
    var self = this;

    self.logger.info(channel, sender, body);

    var command = null;
    var args = null;

    if (body.substr(0, 1) == '.') {
        var parts = body.substr(1).split(' ', 2);
        command = 'cmds/' + parts[0];
        args = body.substr(parts[0].length + 2);
    } else if (body.indexOf('@' + this.bot.mention_name) == 0) {
        var parts = body.substr(this.bot.mention_name.length + 2).split(' ', 2);
        command = 'cmds/' + parts[0];
        args = body.substr(this.bot.mention_name.length + 2 + parts[0].length + 1);
    }

    var m;
    if (command) {
        var roomId = self.getRoomIdByJid(channel);
        var message = null;
        if (fs.existsSync(command)) {
            var child = child_process.exec(command + ' "' + args + '"', function(err, stdout, stderr) {
                if (!err) {
                    message = '@' + sender.split(' ')[0] + ' ' + stdout;
                } else {
                    message = '@' + sender.split(' ')[0] + ' command exited status ' + err.code;
                }

                if (message) {
                    if (roomId) {
                        self.sendMessage(message, {roomId: roomId, from: 'Bot', notify: 0});
                    } else {
                        self.bot.message(channel, message);
                    }
                }
            });
        } else {
            message = '@' + sender.split(' ')[0] + ' No such command';
            if (message) {
                if (roomId) {
                    self.sendMessage(message, {roomId: roomId, from: 'Bot', notify: 0});
                } else {
                    self.bot.message(channel, message);
                }
            }
        }
    } else if ((m = body.match(/(\bhttps?:\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gi)) && m.length > 0 && body.indexOf('<a ') == -1) {
        var roomId = self.getRoomIdByJid(channel);
        var message = '';
        for (var i = 0; i < m.length; i++) {
            //if (m[i].indexOf('https://twitter.com/') == 0) continue;
            message += '<a href="' + m[i] + '">' + m[i] + '</a><br/>';
        }
        if (message.length) {
            if (roomId) {
                self.sendMessage(message, {roomId: roomId, from: 'Bot', notify: 0});
            } else {
                self.bot.message(channel, message);
            }
        }
    }
}

App.prototype.onDatagramReceived = function(buffer, rinfo) {
    var self = this;

    var packet = {};
    var offset = 0;
    packet.version = buffer.readUInt8(offset);
    offset++;

    // id
    var len = buffer.readUInt16BE(offset);
    offset += 2;
    packet.id = buffer.toString('utf8', offset, offset + len);
    offset += len;

    // server
    len = buffer.readUInt16BE(offset);
    offset += 2;
    packet.server = buffer.toString('utf8', offset, offset + len);
    offset += len;

    // type
    len = buffer.readUInt16BE(offset);
    offset += 2;
    packet.type = buffer.toString('utf8', offset, offset + len);
    offset += len;

    // payload
    len = buffer.readUInt16BE(offset);
    offset += 2;
    packet.payload = buffer.toString('utf8', offset, offset + len);
    offset += len;

    if (this.checkReceived(packet.id)) {
        return;
    }

    if (packet.type == 'report') {
        var payload = JSON.parse(packet.payload);
        //var message = '[Report] 通報者: ' + payload['reporter']['name'] + ' 通報対象者: ' + payload['reported']['name'] + ' サーバ: ' + payload['server'] + ' 通報理由: ' + payload['reason'];
        //self.bot.message(this.config.xmpp.defaultChannel, message);
        var message = '通報者: ' + payload['reporter']['name'] + '<br/>通報対象者: ' + payload['reported']['name'] + '<br/>サーバ: ' + payload['server'] + '<br/>通報理由: ' + payload['reason'];
        this.sendMessage(message, {from: 'Report', notify: 1});
    } else if (packet.type == 'punishment') {
        var payload = JSON.parse(packet.payload);
        var level = '不明';
        if (payload['level'] == 1) {
            level = '警告';
        } else if (payload['level'] == 2) {
            level = 'キック';
        } else if (payload['level'] == 3) {
            level = 'BAN';
        } else if (payload['level'] == 4) {
            level = '無期限BAN';
        }
        //var message = '[Punishment] 処罰者: ' + payload['punisher'] + ' 処罰対象者: ' + payload['punished'] + ' 処罰レベル: ' + level + ' 理由: ' + payload['reason'];
        //self.bot.message(this.config.xmpp.defaultChannel, message);
        var message = '処罰者: ' + payload['punisher'] + '<br/>処罰対象者: ' + payload['punished'] + '<br/>処罰レベル: ' + level + '<br/>理由: ' + payload['reason'];
        this.sendMessage(message, {from: 'Punishment', notify: 0});
    } else if (packet.type == 'custom') {
        var payload = JSON.parse(packet.payload);
        //var message = '[' + payload['type'] + '] ' + payload['message'];
        //self.bot.message(this.config.xmpp.defaultChannel, message);
        this.sendMessage(payload['message'], {from: payload['type'], notify: 1});
    }
}

App.prototype.sendMessage = function(message, options) {
    var self = this;
    if (options.autoLink == undefined || options.autoLink) {
        message = message.replace(/(\bhttps?:\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gi, "<a href='$1'>$1</a>");
    }
    var data = {
        room_id: options.roomId ? options.roomId : this.config.defaultRoom,
        from: options.from ? options.from : 'Bot',
        message: message,
        notify: options.notify ? 1 : 0
    };
    request.post({url: 'https://api.hipchat.com/v1/rooms/message?format=json&auth_token=' + this.config.token, form: data}, function(err, httpResponse, body) {
        try {
            var response = JSON.parse(body);
            if (response.status != 'sent') {
                self.logger.warn(response);
            }
        } catch (e) {
            self.logger.warn(e);
        }
    });
}

App.prototype.getRoomIdByJid = function(jid) {
    for (var i = 0; i < this.config.rooms.length;i ++) {
        if (this.config.rooms[i].jid == jid) {
            return this.config.rooms[i].id;
        }
    }
    return null;
}

App.prototype.checkReceived = function(id) {
    for (var i = 0; i < this.receivedIds.length; i++) {
        if (id == this.receivedIds[i]) {
            return true;
        }
    }

    this.receivedIds.push(id);
    if (this.receivedIds.length > 100) {
        this.receivedIds.shift();
    }
}

var app = new App(config);
app.run();
